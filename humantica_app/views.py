from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import http.client
import json
import os
import requests
import time
import pandas as pd
from re import sub
import io
# Create your views here.

@csrf_exempt
def home(request):
    """
    :param request:
    :return:
    """
    if request.method == 'POST':
        input_text = request.POST['input_text']

        status_code_1 = create_analysys(input_text)
        # if status_code_1 == 200:
        time.sleep(5)
        status_code_2, response_text = fetch_analysys()
        persona_anlysis_dict = eval(response_text)

        disc_assessment = persona_anlysis_dict['results']['personality_analysis']['disc_assessment']
        new_l_d_a = []
        for i in disc_assessment:
            new_d = {}
            new_d['axis'] = i.upper().title()
            new_d['value'] = disc_assessment[i]['score']
            new_l_d_a.append(new_d)
        df_1 = pd.DataFrame(disc_assessment)

        summary = persona_anlysis_dict['results']['personality_analysis']['summary']
        # new_l_summary = []
        # for i in summary:
        #     new_d = {}
        #     new_d['axis'] = i
        #     new_d['value'] = summary[i]['score']
        #     new_l_summary.append(new_d)
        df_2 = pd.DataFrame(summary)

        ocean_assessment = persona_anlysis_dict['results']['personality_analysis']['ocean_assessment']
        new_l_o_a = []
        for i in ocean_assessment:
            new_d = {}
            new_d['axis'] = i.upper().title()
            new_d['value'] = ocean_assessment[i]['score']
            new_l_o_a.append(new_d)
        df_3 = pd.DataFrame(ocean_assessment)

        str_io1 = io.StringIO()
        df_1.columns = map(str.title, df_1.columns)
        df_1 = df_1.apply(lambda x: x.astype(str).str.title())
        df_1.to_html(buf=str_io1, classes='table table-striped projectSpreadsheet table_1')
        df_1_html_str = str_io1.getvalue()

        str_io2 = io.StringIO()
        df_2.columns = map(str.upper, df_2.columns)
        df_2 = df_2.apply(lambda x: x.astype(str).str.title())
        df_2.to_html(buf=str_io2, classes='table table-striped projectSpreadsheet table_2')
        df_2_html_str = str_io2.getvalue()

        str_io3 = io.StringIO()
        df_3.columns = map(str.title, df_3.columns)
        df_3 = df_3.apply(lambda x: x.astype(str).str.title())
        df_3.to_html(buf=str_io3, classes='table table-striped projectSpreadsheet table_3')
        df_3_html_str = str_io3.getvalue()

        return render(request, 'home.html', {
            'disc_assessment': [new_l_d_a],
            # 'summary': new_l_summary,
            'ocean_assessment': [new_l_o_a],
            'df_1_html_str': df_1_html_str,
            'df_2_html_str': df_2_html_str,
            'df_3_html_str': df_3_html_str
        })
    else:
        return render(request, 'home.html')


def create_analysys(input_text):
    # Create instance to connect to humantic server
    conn = http.client.HTTPSConnection("api.humantic.ai")

    CREATE_ENDPOINT = "/v1/user-profile/create"  # CREATE endpoint
    headers = {
        'Content-Type': 'application/json'
    }

    # API Key: required, Get the api key from environment variable or substitute it directly
    # New_API_KEY: chrexec_402b6b926351252d23041a50af30bbb8
    # Old_API_KEY: chrexec_7f9a26ba3e35f383ab80d8fcbc8f562c
    API_KEY = "chrexec_402b6b926351252d23041a50af30bbb8"

    # Analysis ID: required; User profile link from LinkedIn or, User Email ID
    # or, for document or text, use any unique identifier. We suggest using a value that helps you identify the analysis easily.
    # USER_ID = "https://www.linkedin.com/in/ramanaditya"  # or, any unique identifier

    url = f"{CREATE_ENDPOINT}?apikey={API_KEY}&id=ata_demo_id1"

    payload = json.dumps({
        "text": input_text
    })

    conn.request("POST", url, payload, headers)
    response = conn.getresponse()
    status_code = response.status
    data = response.read()
    return status_code


def fetch_analysys():
    BASE_URL = "https://api.humantic.ai/v1/user-profile"  # Base URL for the FETCH endpoint
    headers = {
        'Content-Type': 'application/json'
    }

    # API Key: required; get the API key from the environment variable or substitute it directly
    API_KEY = "chrexec_402b6b926351252d23041a50af30bbb8"

    # Analysis ID: required; should be same as the id used to create the analysis
    # USER_ID = "https://www.linkedin.com/in/ramanaditya"  # or, any unique identifier

    # Persona: optional; possible values: "sales", "hiring"
    # PERSONA = "sales"

    url = f"{BASE_URL}?apikey={API_KEY}&id=ata_demo_id1"

    response = requests.request("GET", url, headers=headers)

    return response.status_code, response.text
