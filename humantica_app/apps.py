from django.apps import AppConfig


class HumanticaAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'humantica_app'
